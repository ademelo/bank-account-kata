package com.oxiane.bank.service;

import com.oxiane.bank.exception.AccountNotFoundException;
import com.oxiane.bank.model.domain.Account;

import java.util.Optional;

public interface AccountService {

    void updateAccountPosition(Account account);

    Optional<Account> getAccount(int accountNumber);

    Double getAccountPosition(Integer accountNumber) throws AccountNotFoundException;
}
