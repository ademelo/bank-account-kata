package com.oxiane.bank.service.impl;

import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.NewOperationRequest;
import com.oxiane.bank.model.domain.Operation;
import com.oxiane.bank.repository.OperationRepository;
import com.oxiane.bank.service.AccountService;
import com.oxiane.bank.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.DoubleFunction;

@Service("operation_service")
public class OperationServiceImpl implements OperationService {

    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    private AccountService accountService;

    private static final DoubleFunction<String> evaluateOperationType = operation -> operation > 0 ? "deposit" : "withdrawal";

    @Transactional
    public Operation registerOperation(NewOperationRequest newOperationRequest, Account account) {
        accountService.updateAccountPosition(account);
        Double operationAmount = newOperationRequest.getOperationAmount();
        Operation operation = new Operation(evaluateOperationType.apply(operationAmount), operationAmount, account);
        return operationRepository.save(operation);
    }

    public List<Operation> getAllOperationForAccount(Account account, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC,"date"));
        return operationRepository.findByAccount(account, pageRequest);
    }
}
