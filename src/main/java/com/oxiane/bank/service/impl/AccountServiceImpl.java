package com.oxiane.bank.service.impl;

import com.oxiane.bank.exception.AccountNotFoundException;
import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.repository.AccountRepository;
import com.oxiane.bank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("account_service")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public void updateAccountPosition(Account account) {
        accountRepository.save(account);
    }

    public Optional<Account> getAccount(int accountNumber) {
        return accountRepository.getAccountByAccountNumber(accountNumber);
    }

    @Override
    public Double getAccountPosition(Integer accountNumber) throws AccountNotFoundException {
        Double accountPosition = accountRepository.getPositionForAccountNumber(accountNumber);
        if(accountPosition == null) throw new AccountNotFoundException(String.valueOf(accountNumber));
        return accountPosition;
    }
}
