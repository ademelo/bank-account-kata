package com.oxiane.bank.service;

import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.NewOperationRequest;
import com.oxiane.bank.model.domain.Operation;

import java.util.List;

public interface OperationService {

    Operation registerOperation(NewOperationRequest newOperationRequest, Account account);

    List<Operation> getAllOperationForAccount(Account account, int page, int size);
}
