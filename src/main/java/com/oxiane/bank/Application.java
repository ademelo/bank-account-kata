package com.oxiane.bank;

import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.domain.Client;
import com.oxiane.bank.model.RoleName;
import com.oxiane.bank.repository.AccountRepository;
import com.oxiane.bank.repository.ClientRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(AccountRepository accountRepository, ClientRepository clientRepository) {
        return args -> {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encodedPassword = passwordEncoder.encode("pwd");
            Client client = clientRepository.save(new Client("Toto", "Titi", 208, "email", encodedPassword, RoleName.ROLE_USER));
            accountRepository.save(new Account(1001, 0d, client));
            accountRepository.save(new Account(1002, 0d, client));
        };
    }

}
