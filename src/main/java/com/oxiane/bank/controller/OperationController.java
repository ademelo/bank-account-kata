package com.oxiane.bank.controller;

import com.oxiane.bank.exception.AccountNotFoundException;
import com.oxiane.bank.exception.InsufficientFundException;
import com.oxiane.bank.model.AccountPositionResponse;
import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.ApiError;
import com.oxiane.bank.model.NewOperationRequest;
import com.oxiane.bank.model.domain.Operation;
import com.oxiane.bank.model.hateoas.OperationResource;
import com.oxiane.bank.model.hateoas.OperationResourceAssembler;
import com.oxiane.bank.service.AccountService;
import com.oxiane.bank.service.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Slf4j
@RestController
@RequestMapping("/operations")
public class OperationController {

    @Autowired
    private OperationService operationService;

    @Autowired
    private AccountService accountService;

    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Operation> executeOperation(@Validated @RequestBody NewOperationRequest newOperationRequest,
                                           Authentication auth) throws AccountNotFoundException, InsufficientFundException {

        LOGGER.info("executeOperation() newOperationRequest: {}, for Client: {}, Name: {}", newOperationRequest, auth.getName());

        Optional<Account> optionalAccount = accountService.getAccount(newOperationRequest.getAccountNumber());

        //Account account = optionalAccount.orElseThrow(new AccountNotFoundException(String.valueOf(newOperationRequest.getAccountNumber())));

        if (! optionalAccount.isPresent()) {
            throw new AccountNotFoundException(String.valueOf(newOperationRequest.getAccountNumber()));
        }
        Account account = optionalAccount.get();

        String surname = account.getClient().getSurname().toLowerCase();
        if (! surname.equalsIgnoreCase(auth.getName())) {
            throw new AccessDeniedException("No access for this resource");
        }

        Double newAccountPosition = account.getAccountPosition() + newOperationRequest.getOperationAmount();

        if (newAccountPosition < 0) {
           throw new InsufficientFundException(String.valueOf(newOperationRequest.getAccountNumber()));
        }

        account.setAccountPosition(newAccountPosition);
        LOGGER.info("Account {} position updated", account.getAccountNumber());
        Operation operation = operationService.registerOperation(newOperationRequest, account);
        LOGGER.info("New Operation registered for account {}", account.getAccountNumber());

        return new ResponseEntity<>(operation, HttpStatus.CREATED);
    }

    @GetMapping("/{operationId}")
    public ResponseEntity<OperationResource> getOperationInfo(
                                                    @PathVariable("operationId") final int operationId,
                                                    Authentication auth) {
        LOGGER.info("getOperationInfo() operationId: {}", operationId);

        return null;
    }

    @GetMapping
    public ResponseEntity<Resources<OperationResource>> getAllOperationForAClient(
                                                    @RequestParam(value = "accountNumber", required = true) int accountNumber,
                                                    @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                    @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                                                    Authentication auth) throws AccountNotFoundException {

        LOGGER.info("getAllOperationForAClient() accountNumber: {}", accountNumber);

        Optional<Account> optionalAccount = accountService.getAccount(accountNumber);
        if (! optionalAccount.isPresent()) {
            throw new AccountNotFoundException(String.valueOf(accountNumber));
        }
        Account account = optionalAccount.get();

        String surname = account.getClient().getSurname().toLowerCase();
        if (! surname.equalsIgnoreCase(auth.getName())) {
            throw new AccessDeniedException("No access for this resource");
        }

        List<Operation> operationHistory = operationService.getAllOperationForAccount(account, page, size);

        //Resources<Resource<Operation>> resources = Resources.wrap(operationHistory);
        List<OperationResource> operationResources = new OperationResourceAssembler().toResources(operationHistory);
        Resources<OperationResource> resources = new Resources<>(operationResources);

        resources.add(
                linkTo(methodOn(OperationController.class).getAllOperationForAClient(accountNumber, page, size, auth))
                    .withRel("allOperation")
        );

        return ResponseEntity.status(HttpStatus.OK).body(resources);
    }

    @GetMapping(path = "/{accountNumber}/position", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAccountPosition(@PathVariable("accountNumber") final Integer accountNumber) throws AccountNotFoundException {

        LOGGER.info("getAccountPosition() accountNumber: {}", accountNumber);

        Double position = accountService.getAccountPosition(accountNumber);

        AccountPositionResponse accountPositionResponse = new AccountPositionResponse(position);

        return ResponseEntity.status(HttpStatus.OK).body(accountPositionResponse);
    }


    @ExceptionHandler(InsufficientFundException.class)
    @ResponseBody
    public ResponseEntity<ApiError> handleInsufficientFundException(final InsufficientFundException ex) {
        LOGGER.error("Error, insufficient fund", ex);
        return new ResponseEntity<>(
                new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ex.getMessage()), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseBody
    public ResponseEntity<ApiError> handleAccountNotFoundException(final AccountNotFoundException ex) {
        LOGGER.error("Error, account not found", ex);
        return new ResponseEntity<>(
                new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), ex.getMessage()), HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public ResponseEntity<ApiError> handleAccessDeniedException(final AccessDeniedException ex) {
        LOGGER.error("Error, access denied for account", ex);
        return new ResponseEntity<>(
                new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), ex.getMessage()), HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler({ MethodArgumentNotValidException.class })
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, WebRequest request) {
        LOGGER.error("handleMethodArgumentNotValid() - MethodArgumentNotValidException: {}", ex.getMessage());

        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

}
