package com.oxiane.bank.repository;

import com.oxiane.bank.model.domain.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {

    Optional<Client> findBySurnameOrEmail(String usernameOrEmail, String usernameOrEmail1);

}
