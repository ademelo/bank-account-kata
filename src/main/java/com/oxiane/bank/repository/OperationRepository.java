package com.oxiane.bank.repository;

import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.domain.Operation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends CrudRepository<Operation, Integer> {

    List<Operation> findByAccount(Account account, Pageable pageable);

}
