package com.oxiane.bank.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class NewOperationRequest {
    @NotNull
    private Integer accountNumber;
    @NotNull
    private Double operationAmount;

    public NewOperationRequest(Integer accountNumber, Double operationAmount) {
        this.accountNumber = accountNumber;
        this.operationAmount = operationAmount;
    }
}
