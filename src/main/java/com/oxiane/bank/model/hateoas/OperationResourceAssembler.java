package com.oxiane.bank.model.hateoas;

import com.oxiane.bank.controller.OperationController;
import com.oxiane.bank.model.domain.Operation;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class OperationResourceAssembler extends ResourceAssemblerSupport<Operation, OperationResource> {

    public OperationResourceAssembler() {
        super(OperationController.class, OperationResource.class);
    }

    @Override
    public OperationResource instantiateResource(Operation operation) {
        return new OperationResource(operation);
    }

    @Override
    public OperationResource toResource(Operation operation) {
        return createResourceWithId(operation.getOperationId(), operation);
    }
}
