package com.oxiane.bank.model.hateoas;

import com.oxiane.bank.model.domain.Operation;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

@Getter
public class OperationResource extends ResourceSupport {

    private String operationType;
    private Double operationValue;
    private Date date;
    private Double balance;

    public OperationResource(Operation operation) {
        this.operationType = operation.getOperationType();
        this.operationValue = operation.getOperationValue();
        this.date = operation.getDate();
        this.balance = operation.getBalance();
    }

}
