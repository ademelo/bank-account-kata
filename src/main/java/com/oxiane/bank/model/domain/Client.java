package com.oxiane.bank.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxiane.bank.model.RoleName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "client_id")
    private Integer clientId;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "surname")
    private String surname;
    @Column(name = "client_number")
    private Integer clientNumber;
    @Column(name = "email")
    private String email;
    @JsonIgnore
    @Column(name = "password")
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private RoleName role;

    public Client(String firstname, String surname, Integer clientNumber, String email, String password, RoleName role) {
        this.firstname = firstname;
        this.surname = surname;
        this.clientNumber = clientNumber;
        this.email = email;
        this.password = password;
        this.role = role;
    }

}
