package com.oxiane.bank.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "account_id")
    private Integer accountId;
    @Column(unique = true, nullable = false)
    private Integer accountNumber;
    @Column(nullable = false)
    private Double accountPosition;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    public Account(Integer accountNumber, Double accountPosition, Client client) {
        this.accountNumber = accountNumber;
        this.accountPosition = accountPosition;
        this.client = client;
    }
}
