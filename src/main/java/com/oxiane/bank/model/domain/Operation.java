package com.oxiane.bank.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxiane.bank.model.domain.Account;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AccessLevel;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "operation_id")
    private Integer operationId;
    @Column(name = "operation_type")
    private String operationType;
    @Column(name = "operation_value")
    private Double operationValue;
    @Column(name = "operation_date")
    private Date date;
    @Column(name = "balance")
    private Double balance;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "account_id")
    private Account account;

    public Operation(String operationType, Double operationValue, Account account) {
        this.operationType = operationType;
        this.operationValue = operationValue;
        this.account = account;
        this.balance = account.getAccountPosition();
        this.date = new Date();
    }

}
