package com.oxiane.bank.model;

import lombok.*;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AccountPositionResponse {
    private Double accountPosition;

    public AccountPositionResponse(Double accountPosition) {
        this.accountPosition = accountPosition;
    }
}
