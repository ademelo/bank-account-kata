package com.oxiane.bank;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql({"classpath:truncate-data.sql", "classpath:test-data.sql"})
public abstract class SpringBootBaseIT {

    private String BASE_URL = "http://localhost:";
    private String accessToken = "";

    @LocalServerPort
    protected int port;

    @Before
    public void setUp() throws IOException, JSONException {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"usernameOrEmail\":\"test\", \"password\":\"pwd\"} ");

        Request request = new Request.Builder()
                .url(BASE_URL + port + "/api/auth/signin")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        com.squareup.okhttp.Response response = client.newCall(request).execute();
        JSONObject jsonAccessToken = new JSONObject(response.body().string());
        accessToken = jsonAccessToken.getString("accessToken");
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    public String getAccessToken() {
        return accessToken;
    }

}
