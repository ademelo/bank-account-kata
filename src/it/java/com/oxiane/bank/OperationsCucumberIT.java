package com.oxiane.bank;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.test.context.jdbc.Sql;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/it/resources/features/get-account-position.feature")
@Sql({"classpath:truncate-data.sql", "classpath:test-data.sql"})
public class OperationsCucumberIT {
}
