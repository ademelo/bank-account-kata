package com.oxiane.bank;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Ignore
public class OperationsStepDefinitions extends SpringBootBaseIT {

    private Response response;
    private ValidatableResponse json;
    private RequestSpecification request;

    @LocalServerPort
    private int port;

    @Autowired
    private DataSource ds;


    @Given("^an account exist with the number (.*)")
    public void the_client_issues_GET_account_position(String accountNumber) throws Throwable {
        ScriptUtils.executeSqlScript(ds.getConnection(), new ClassPathResource("truncate-data.sql"));
        ScriptUtils.executeSqlScript(ds.getConnection(), new ClassPathResource("test-data.sql"));
        setUp();
        request = given()
                .headers(
                        "Authorization",
                        "Bearer " + getAccessToken(),
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .pathParam("accountNumber", accountNumber);
    }

    @When("^a client asks for his account position")
    public void a_client_asks_for_his_account_position() throws Throwable {
        response = request.when().get(getBaseUrl() + port + "/operations/{accountNumber}/position");
    }

    @Then("^the response status should be (\\d+)")
    public void verify_status_code(int statusCode) {
        json = response.then().statusCode(statusCode);
    }

    @And("^the response should include value$")
    public void response_equals(Map<String, String> responseFields){
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if(StringUtils.isNumeric(field.getValue())){
                json.body(field.getKey(), equalTo(Float.parseFloat(field.getValue())));
            }
            else{
                json.body(field.getKey(), equalTo(field.getValue()));
            }
        }
        //json.body(".accountPosition",equalTo(Float.valueOf(accountPosition)));
    }

    @Given("^an account that does not exist")
    public void the_client_issues_GET_account_position() throws Throwable {
        ScriptUtils.executeSqlScript(ds.getConnection(), new ClassPathResource("truncate-data.sql"));
        ScriptUtils.executeSqlScript(ds.getConnection(), new ClassPathResource("test-data.sql"));
        setUp();
        request = given()
                .headers(
                        "Authorization",
                        "Bearer " + getAccessToken(),
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .pathParam("accountNumber", "123");
    }
}
