package com.oxiane.bank;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.json.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql({"classpath:truncate-data.sql", "classpath:test-data.sql"})
public class OperationControllerIT {

    private static final String ACCOUNT_NUMBER = "1003";
    private static final String BAD_ACCOUNT_NUMBER = "888";
    private static final String BASE_URL = "http://localhost:";
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String ACCEPT = "Accept";

    private String accessToken = "";

    @LocalServerPort
    private int port;

    @Before
    public void setUp() throws IOException, JSONException {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"usernameOrEmail\":\"test\", \"password\":\"pwd\"} ");

        Request request = new Request.Builder()
                .url(BASE_URL + port + "/api/auth/signin")
                .post(body)
                .addHeader(CONTENT_TYPE, "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        com.squareup.okhttp.Response response = client.newCall(request).execute();
        JSONObject jsonAccessToken = new JSONObject(response.body().string());
        accessToken = jsonAccessToken.getString("accessToken");
    }


    @Test
    public void should_get_operations_for_a_client_account() {
        given()
                .headers(
                        AUTHORIZATION,
                        BEARER + accessToken,
                        CONTENT_TYPE,
                        ContentType.JSON,
                        ACCEPT,
                        ContentType.JSON)
        .when().get(BASE_URL + port + "/operations?accountNumber=" + ACCOUNT_NUMBER)
                .then()
                .statusCode(200)
                .body("[0].operationType", equalTo("withdrawal"))
                .body("[0].operationValue", equalTo(-20f))
                .body("[0].date",equalTo("2018-11-04T17:47:52.690+0000"))
                .body("[0].balance",equalTo(30f));
    }


    @Test
    public void should_get_the_right_account_balance_for_multiple_operations() {
        given()
                .headers(
                        AUTHORIZATION,
                        BEARER + accessToken,
                        CONTENT_TYPE,
                        ContentType.JSON,
                        ACCEPT,
                        ContentType.JSON)
        .when().get(BASE_URL + port + "/operations?accountNumber=" + ACCOUNT_NUMBER)
                .then()
                .statusCode(200)
                .body("[1].balance",equalTo(50f))
                .body("[1].operationType",equalTo("deposit"));
    }


    @Test
    public void should_execute_an_operation_for_a_client() {

        Response response = given()
                .headers(
                        AUTHORIZATION,
                        BEARER + accessToken,
                        CONTENT_TYPE,
                        ContentType.JSON,
                        ACCEPT,
                        ContentType.JSON)
                .with()
                .contentType(ContentType.JSON)
                .body(String.format("{\"accountNumber\":%s,\"operationAmount\":%s}", ACCOUNT_NUMBER, "50"))
                .when()
                .post(BASE_URL + port + "/operations/");
        response
                .then()
                .statusCode(201)
                .body("operationType", equalTo("deposit"))
                .body("operationValue", equalTo(50f));
    }


    @Test
    public void should_return_forbidden_when_executing_operation_on_an_unauthorized_resource() {
        Response response = given()
                .headers(
                        AUTHORIZATION,
                        BEARER + accessToken,
                        CONTENT_TYPE,
                        ContentType.JSON,
                        ACCEPT,
                        ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(String.format("{\"accountNumber\":%s,\"operationAmount\":%s}", BAD_ACCOUNT_NUMBER, "50"))
                .when()
                .post(BASE_URL + port + "/operations/");
        response
                .then()
                .statusCode(403);
    }
}
