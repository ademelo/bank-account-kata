Feature: A client can access to his account position
  Scenario: client makes call to GET accountPosition
    Given an account exist with the number 1003
    When a client asks for his account position
    Then the response status should be 200
    And the response should include value
      | accountPosition 	 	| 150 				|

  Scenario: client makes call to GET accountPosition
    Given an account that does not exist
    When a client asks for his account position
    Then the response status should be 404