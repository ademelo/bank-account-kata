INSERT INTO Client (client_id,firstname, surname, client_number, email, password, role) VALUES (1,'PRENOM','test',123,'email','$2a$04$IHTsKYPCTOBMl5YDljZn1e4FHPppIzkdn7SK6sqORTsiyQK597Wi.','ROLE_USER');
INSERT INTO Client (client_id,firstname, surname, client_number, email, password, role) VALUES (2,'PRENOM','client2',777,'email','$2a$04$IHTsKYPCTOBMl5YDljZn1e4FHPppIzkdn7SK6sqORTsiyQK597Wi.','ROLE_USER');
INSERT INTO Account (account_number, account_position, client_id) VALUES (1003,150,
select client_id from Client where client_number=123
);
INSERT INTO Account (account_number, account_position, client_id) VALUES (888,150,
select client_id from Client where client_number=777
);
INSERT INTO Operation (operation_type, operation_value, operation_date, balance, account_id) VALUES ('deposit',50,parsedatetime('04-10-2018 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'),50,
select account_id from Account where account_number=1003
);
INSERT INTO Operation (operation_type, operation_value, operation_date, balance, account_id) VALUES ('withdrawal',-20,parsedatetime('04-11-2018 18:47:52.690', 'dd-MM-yyyy hh:mm:ss.SS'),30,
select account_id from Account where account_number=1003
);
