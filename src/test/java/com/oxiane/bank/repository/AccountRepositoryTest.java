package com.oxiane.bank.repository;

import com.oxiane.bank.Application;
import com.oxiane.bank.model.RoleName;
import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.domain.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class })
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ClientRepository clientRepository;

    private static Integer ACCOUNT_NUMBER = 1008;

    @Before
    public void setUp() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode("pwd");
        Client client = new Client("Test", "Test", 2008, "email", encodedPassword, RoleName.ROLE_USER);
        Account account = new Account(ACCOUNT_NUMBER, 100d, client);
        clientRepository.save(client);
        accountRepository.save(account);
    }

    @Test
    public void should_get_account_position_for_a_known_account_number() {
        assertThat(accountRepository.getPositionForAccountNumber(ACCOUNT_NUMBER), is(100d));
    }

    @Test//(expected = AuthenticationException.class)
    public void should_throw_error_for_an_unknown_account() {
        assertThat(accountRepository.getPositionForAccountNumber(123456), is(nullValue()));
    }

}
