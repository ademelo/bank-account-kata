package com.oxiane.bank.service;

import com.oxiane.bank.model.*;
import com.oxiane.bank.model.domain.Account;
import com.oxiane.bank.model.domain.Client;
import com.oxiane.bank.model.domain.Operation;
import com.oxiane.bank.repository.OperationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations="classpath:application-test.properties")
public class OperationServiceTest {

    @Autowired
    private OperationService operationService;

    @MockBean
    private AccountService accountService;

    @MockBean
    private OperationRepository operationRepository;

    private Client client = new Client("Toto", "Titi", 1, "email","pwd", RoleName.ROLE_USER);

    @Test
    public void should_register_operation() {
        Account account = new Account(1001,50d, client);
        Operation expectedOperation = new Operation("deposit", 50d, account);
        NewOperationRequest newOperationRequest = new NewOperationRequest(1001, 50d);

        doNothing()
                .when(accountService)
                .updateAccountPosition(account);
        when(operationRepository.save(any()))
                .thenReturn(expectedOperation);

        Operation insertedOperation = operationService.registerOperation(newOperationRequest, account);

        assertThat(insertedOperation, is(expectedOperation));
    }

}
