package com.oxiane.bank.service;

import com.oxiane.bank.exception.AccountNotFoundException;
import com.oxiane.bank.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations="classpath:application-test.properties")
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    private static Integer ACCOUNT_NUMBER = 1008;


    @Test
    public void should_get_account_position_for_account_number() throws AccountNotFoundException {
        when(accountRepository.getPositionForAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(50d);
        assertThat(accountService.getAccountPosition(ACCOUNT_NUMBER), is(50d));
    }

    @Test(expected = AccountNotFoundException.class)
    public void should_throw_account_not_found_when_trying_to_get_account_position_for_unknown_account_number() throws AccountNotFoundException {
        when(accountRepository.getPositionForAccountNumber(ACCOUNT_NUMBER))
                .thenReturn(null);
        accountService.getAccountPosition(ACCOUNT_NUMBER);
    }

}
