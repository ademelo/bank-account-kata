# Kata bank-account

This project was created with Spring Boot and use an embedded database (h2), and openjdk 10.0.2

## Getting Started

To run this project, please follow those instructions

### Prerequises

Clone this repository:

```text
    git clone git@framagit.org:ademelo/bank-account-kata.git
```

Then open the project with the IDE of your choice, Right click on the main class named Application, and then press "run Application".

You can also execute the project with maven:
```text
    mvn package && java -jar target/bank-account-api-1.0-SNAPSHOT.jar
```

To execute the unit tests alone:
```text
    mvn test
```

To build a docker container with maven:
```text
    sudo mvn install dockerfile:build
```
To run the dockerised API:
```text
    sudo docker run -p 8080:8080 -t oxiane/bank-account-api
```


## Documentation

When the server is running you can access the API Swagger at this URL:

```text
    http://localhost:8080/swagger-ui.html
```

## API

With this API, you can register deposit and withdrawal operations on your bank account.

When the API is running, an embedded Apache Tomcat Server will be running at :  

```text
    http://localhost:8080/
```  
First of all, you need to get a user token with your credentials

With CURL :
```text
curl -X POST \
  http://localhost:8080/api/auth/signin \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{"usernameOrEmail":"Titi", "password":"pwd"} '
```  

Then you are able to make POST and GET request on the bank-account-api with an Authorization Bearer and the user token.

You can make POST Request at this URL in order to make a deposit or a withdrawal (testing account is 1001):  

```text
    http://localhost:8080/operations
```  

With CURL :  

```CURL
    curl -X POST \
      http://localhost:8080/operations/ \
      -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTQxNjQwNDA5LCJleHAiOjE1NDIyNDUyMDl9.q0MajuoSR7DTDD4AoZxHzjosoXbmRAx-BKBb03rx1xRv8Oj4rx2Sryn5UAD4GDsJw193O8qCpU2PrMjiNXIyhA' \
      -H 'Content-Type: application/json' \
      -H 'cache-control: no-cache' \
      -d '{"accountNumber": 1001, "operationAmount": 5000}'
```  

You can make GET Request at this URL in order to retrieve your operation history :  

```text
    http://localhost:8080/operations?accountNumber=1001&page=0&size=2
```  

With CURL :  

```CURL
    curl -X GET \
      'http://localhost:8080/operations?accountNumber=1001&page=0&size=2' \
      -H 'Authorization: Bearer  eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNTQxNjM1MjAyLCJleHAiOjE1NDIyNDAwMDJ9.XyVr-Mf9hSr4lCDWIYvtSkWKHY69wxMdARVw_nvitqu_RCFgwlK-7UZ-EurPjP7s5fAt2FEwCnlH-DhtY0oCWQ' \
      -H 'cache-control: no-cache'
```  


